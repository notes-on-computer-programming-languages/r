ranks <- read.csv("rank.csv")
print(ranks)
print(is.data.frame(ranks))
print(ncol(ranks))
print(nrow(ranks))
str(ranks)
print(summary(ranks))

str(as.Date(ranks$date))
print(as.Date(ranks$date))
print(summary(as.Date(ranks$date)))

str(ranks$rank)
print(ranks$rank)
print(summary(ranks$rank))

svg("rank.svg") # , width=3, height=2 # Given width and height were giving bad results.
plot(
    data.frame(
        as.Date(ranks$date),
        rank = ranks$rank
    ),
    xlab = ""
)
dev.off()

str(ranks$referenced_by)
print(ranks$referenced_by)
print(summary(ranks$referenced_by))

svg("referenced_by.svg")
plot(
    data.frame(
        as.Date(ranks$date),
        referenced_by = ranks$referenced_by
    ),
    xlab = ""
)
dev.off()


plot(
    as.Date(ranks$date),
    ranks$rank,
    xlab = ""
)
