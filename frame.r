emp.data <- data.frame(
   emp_id = c (1:5), 
   emp_name = c("Rick", "Dan", "Michelle", "Ryan", "Gary"),
   salary = c(623.3, 515.2, 611.0, 729.0, 843.25), 
   
   start_date = as.Date(c("2012-01-01", "2013-09-23", "2014-11-15", "2014-05-11",
      "2015-03-27")),
   stringsAsFactors = FALSE
)
print(emp.data)
str(emp.data)
print(summary(emp.data))
print(data.frame(emp.data$emp_name,emp.data$salary))
print(emp.data[1:2,])
print(emp.data[c(3,5),c(2,4)])
svg("frame.svg")
plot(emp.data$start_date, emp.data$salary)
dev.off()
# https://www.tutorialspoint.com/r/r_data_frames.htm