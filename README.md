# r

GNU R statistical computation and graphics system. [R (programming language)](https://en.m.wikipedia.org/wiki/R_(programming_language))
* https://r-project.org/

# Unofficial documentation
* [*How to plot a function curve in R*
  ](https://stackoverflow.com/questions/26091323/how-to-plot-a-function-curve-in-r) @stackoverflow
* [*R Programming*](https://en.m.wikibooks.org/wiki/R_Programming) @Wikibooks
* [*8 Graphics Devices*](https://bookdown.org/rdpeng/exdata/graphics-devices.html)
* [*Reading and Writing CSV Files*
  ](https://swcarpentry.github.io/r-novice-inflammation/11-supp-read-write-csv/)
* [*R - CSV Files*](https://www.tutorialspoint.com/r/r_csv_files.htm)
* [*R - Scatterplots*](https://www.tutorialspoint.com/r/r_scatterplots.htm)
* [*Time Series*](https://www.r-graph-gallery.com/time-series.html)
* [*Exporting nice plots in R*](https://www.r-bloggers.com/exporting-nice-plots-in-r/)
  2013-02 Max Gordon

# [Results](https://notes-on-computer-programming-languages.gitlab.io/r)
## Alpine
<figure>
  <img
    src="https://notes-on-computer-programming-languages.gitlab.io/r/alpine/rank.svg"
    alt="Alpine rank"
    width="40%"
  >
  <img
    src="https://notes-on-computer-programming-languages.gitlab.io/r/alpine/referenced_by.svg"
    alt="Alpine referenced_by"
    width="40%"
  >
  <figcaption>Alpine is missing polices</figcaption>
</figure> 

## Debian
<figure>
  <img
    src="https://notes-on-computer-programming-languages.gitlab.io/r/debian/rank.svg"
    alt="Debian rank"
    width="40%"
  >
  <img
    src="https://notes-on-computer-programming-languages.gitlab.io/r/debian/referenced_by.svg"
    alt="Debian referenced_by"
    width="40%"
  >
</figure> 

## Manjaro
<figure>
  <img
    src="https://notes-on-computer-programming-languages.gitlab.io/r/manjaro/rank.svg"
    alt="Manjaro rank"
    width="40%"
  >
  <img
    src="https://notes-on-computer-programming-languages.gitlab.io/r/manjaro/referenced_by.svg"
    alt="Manjaro referenced_by"
    width="40%"
  >
  <figcaption>Manjaro is missing polices</figcaption>
</figure> 

## Rawhide
<figure>
  <img
    src="https://notes-on-computer-programming-languages.gitlab.io/r/rawhide/rank.svg"
    alt="Rawhide rank"
    width="40%"
  >
  <img
    src="https://notes-on-computer-programming-languages.gitlab.io/r/rawhide/referenced_by.svg"
    alt="Rawhide referenced_by"
    width="40%"
  >
</figure> 

## Tumbleweed
<figure>
  <img
    src="https://notes-on-computer-programming-languages.gitlab.io/r/tumbleweed/rank.svg"
    alt="Tumbleweed rank"
    width="40%"
  >
  <img
    src="https://notes-on-computer-programming-languages.gitlab.io/r/tumbleweed/referenced_by.svg"
    alt="Tumbleweed referenced_by"
    width="40%"
  >
</figure> 
